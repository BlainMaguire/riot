'use strict';

angular.module('SummonerStalker', ['angularFlaskServices', 'yaru22.angular-timeago', 'angular-web-notification', 'btford.socket-io'])
    .factory('socket', function (socketFactory) {
      var socket = socketFactory({ioSocket: io.connect('http://localhost:5000')});
      return socket;
    })
    .factory("GamesService", function(){
        return {    summoners : null,
                    getSummoners : function() {
                       return this.summoners;
                    },
                    setSummoners : function(newSummoners) {
                       this.summoners = newSummoners;
                    },
                    setSummoner : function(newSummoner) {
                            angular.forEach(this.summoners, function(summoner) {
                                if(newSummoner.id===summoner.id) {
                                    summoner = newSummoner;
                                }
                            });
                    }
        };
    })
	.config(['$routeProvider', '$locationProvider',
		function($routeProvider, $locationProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'static/partials/landing.html',
			controller: IndexController
		})
		.when('/about', {
			templateUrl: 'static/partials/about.html',
			controller: AboutController
		})
		.when('/summoner', {
			templateUrl: 'static/partials/summoner-list.html',
			controller: SummonerListController
		})
		/* fetch more information on a summoner */
		.when('/summoner/:summonerId', {
			templateUrl: '/static/partials/summoner-detail.html',
			controller: SummonerDetailController
		})
		/* Create a route with different name that will redirect */
		.when('/summoners', {
			templateUrl: 'static/partials/summoner-list.html',
			controller: SummonerListController
		})
		.otherwise({
			redirectTo: '/'
		})
		;

		$locationProvider.html5Mode(true);
	}])
;
