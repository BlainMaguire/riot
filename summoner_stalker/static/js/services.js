'use strict';

angular.module('angularFlaskServices', ['ngResource'])
	.factory('Summoner', function($resource) {
		return $resource('/api/summoner/:summonerId', {}, {
			add: {
			    method: 'POST'
			},
			remove: {
			    method: 'DELETE',
			    params: { summonerId: '' },
			},
			query: {
				method: 'GET',
				params: { summonerId: '' },
				isArray: true
			}
		});
	})
	.factory('Match', function($resource) {
		return $resource('/match/:summonerId', {}, {
			query: {
				method: 'GET',
				params: { summonerId: '' },
			}
		});
	})
;



