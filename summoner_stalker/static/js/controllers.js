'use strict';

/* Controllers */

function IndexController($scope) {
	
}

function AboutController($scope) {
	
}

function SummonerListController($scope, Summoner, socket, GamesService, webNotification) {

    $scope.addSummoner = function() {
            Summoner.add({'name': $scope.inputSummoner}, function(summoner){
            $scope.summoners.push(summoner);
            $scope.inputSummoner = '';
        });
    }
    
    $scope.removeSummoner = function(id) {
            Summoner.remove({ summonerId: id }, function(summoner){
            var index = $scope.summoners.indexOf(summoner);
            $scope.summoners.splice(index, 1);    
        });
    }

    socket.forward('summoner_games', $scope);
    $scope.summoners = GamesService.getSummoners();
    
    if ($scope.summoners === null) {
        var postsQuery = Summoner.get({}, function(summoners) {
		    $scope.summoners = summoners.objects;
		    GamesService.setSummoners($scope.summoners);
	    });
	}
    
    $scope.$on('socket:summoner_games', function (ev, resultsO) {
        // when we get new game results, update our summoners
        var results = resultsO.summoners;
        var notified = !resultsO.init;
        angular.forEach(results, function(result) {
           angular.forEach($scope.summoners, function(summoners) {
           if(result.name===summoners.name) {
                summoners.ingame = result.ingame;
                   if (notified) {
                       webNotification.showNotification('New Game Update', {
                                body: summoners.name +' is '+ (summoners.ingame ? ' in a game! ' : ' no longer in a game.'),
                                // TODO: fetch summoner's profile pic
                                icon: 'http://ddragon.leagueoflegends.com/cdn/6.22.1/img/champion/Akali.png',
                                autoClose: 8000 //auto close the notification
                            }, function onShow(error, hide) {
                            if (error) {
                                window.alert('Unable to show notification: ' + error.message);
                            } else {
                                console.log('Notification Shown.');
                            }
                       });
                   }
           }
           });
        });
        
        GamesService.setSummoners($scope.summoners); //update shared service
    });

}

function SummonerDetailController($scope, $routeParams, Summoner, socket, GamesService, Match) {
    socket.forward('summoner_games', $scope);
    $scope.summoners = GamesService.getSummoners();
    angular.forEach($scope.summoners, function(summoner) {
         if($routeParams.summonerId == summoner.id) {
             $scope.summoner = summoner;
         }
    });
    
    $scope.$on('socket:summoner_games', function (ev, resultsO) {
        var results = resultsO.summoners;
        // when we get new game results, update our summoners
        angular.forEach(results, function(result) {
            // for this summoner?
            if(result.name === $scope.summoner.name) {
                $scope.summoner.ingame = result.ingame;
                GamesService.setSummoner($scope.summoner);
            }
        });
    });
	var postQuery = Summoner.get({ summonerId: $routeParams.summonerId }, function(summoner) {
	    if ($scope.summoner) {
	        $scope.summoner.name = summoner.name;
	        $scope.summoner.watched_date = summoner.watched_date;
	    }
	    else {
		    $scope.summoner = summoner;
		}
		$scope.match_loading = true;
		Match.get({ summonerId: $scope.summoner.api_id }, function(matchDetail) {
            $scope.matchDetail = matchDetail;
            $scope.match_loading = false;
		});
		
	});
}
