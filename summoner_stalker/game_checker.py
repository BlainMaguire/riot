from threading import Thread, Event
thread_stop_event = Event()
import eventlet
eventlet.monkey_patch()

from summoner_stalker import app, socket,socketio
from summoner_stalker.core import api_manager
session = api_manager.session
from summoner_stalker.models import Summoner

from cassiopeia import riotapi, baseriotapi
from cassiopeia.type.api.exception import APIError

riotapi.set_region(app.config['RGAPI_REGION'])
riotapi.set_api_key(app.config['RGAPI_KEY'])
from cassiopeia.type.core.common import LoadPolicy
riotapi.set_load_policy(LoadPolicy.lazy)


i=0
# helper
def get_ingame_summoners():
    global i
    # TODO: calling just .all() here is a bad idea in prod, use top+filter
    summoners_result = session.query(Summoner).all()
    summoners_ingame = []
    for s in summoners_result:
        game = baseriotapi.get_current_game(s.api_id)
        # uncomment the right hand side of next line to test player join+leave
        ingame = game is not None # or (s.name==u'Entranced' and i in [4,5,6,7])
        summoner = {'name':s.name, 'ingame': ingame}
        summoners_ingame.append(summoner)
    i+=1
    return summoners_ingame
    
# for clients connecting for first time, give them current info
def get_current_ingame_status():
    summoners_result = session.query(Summoner).all()
    summoners_ingame = get_ingame_summoners()
    print summoners_ingame
    socket.emit('summoner_games', {'init': True, 'summoners': summoners_ingame})

class SummonerGamesThread(Thread):
    def __init__(self):
        self.delay = 10
        super(SummonerGamesThread, self).__init__()
 
    def run(self):
        """
        Poll the riot api for any changes for our summoners playing a game
        """
        summoners_ingame_old = []
        while True:
            summoners_ingame = get_ingame_summoners()
            delta =  [x for x in summoners_ingame if x not in summoners_ingame_old]
            # only emit if at least one summoner has joined/left a game since
            if (len(delta) > 0):
               socket.emit('summoner_games', {'init': False, 'summoners': delta})
            eventlet.sleep(self.delay)
            summoners_ingame_old = summoners_ingame
