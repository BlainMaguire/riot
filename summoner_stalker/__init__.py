import os
import json
from flask import Flask, request, Response
from flask import render_template, send_from_directory, url_for
from flask_socketio import SocketIO

import logging
from logging.handlers import RotatingFileHandler


app = Flask(__name__)

app.config.from_object('summoner_stalker.settings')

app.url_map.strict_slashes = False

socketio = SocketIO(app)
socket = SocketIO(app, logger=True, engineio_logger=True)

import summoner_stalker.core
import summoner_stalker.models
import summoner_stalker.controllers
