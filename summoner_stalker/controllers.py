import os

import flask
from flask import Flask, request, Response
from flask import render_template, url_for, redirect, send_from_directory
from flask import send_file, make_response, abort

from summoner_stalker import app, socketio, socket
from game_checker import SummonerGamesThread, get_current_ingame_status

# routing for API endpoints, generated from the models designated as API_MODELS
from summoner_stalker.core import api_manager
from summoner_stalker.models import *

import logging
from logging.handlers import RotatingFileHandler

from cassiopeia import riotapi
from cassiopeia.type.api.exception import APIError

riotapi.set_region(app.config['RGAPI_REGION'])
riotapi.set_api_key(app.config['RGAPI_KEY'])
from cassiopeia.type.core.common import LoadPolicy
riotapi.set_load_policy(LoadPolicy.lazy)

from threading import Thread
thread = Thread()

def add_summoner_api_id(data=None):
    if 'name' in data:
        try:
            # library will raise exception if not found
            summoner = riotapi.get_summoner_by_name(data['name'])
            data['api_id'] = summoner.id
        except APIError as error:
            print 'exception while trying to fetch summoner from api', error

for model_name in app.config['API_MODELS']:
    model_class = app.config['API_MODELS'][model_name]
    api_manager.create_api(model_class, methods=['GET', 'POST', 'DELETE'],
                           # Before saving lookup and set rgapi if needed
                           preprocessors={
                           'POST': [add_summoner_api_id]
                           }
    )

session = api_manager.session


# routing for basic pages (pass routing onto the Angular app)
@app.route('/')
@app.route('/about')
@app.route('/summoners')
def basic_pages(**kwargs):
    return make_response(open('summoner_stalker/templates/index.html').read())


# routing for CRUD-style endpoints
# passes routing onto the angular frontend if the requested resource exists
from sqlalchemy.sql import exists

crud_url_models = app.config['CRUD_URL_MODELS']

@app.route('/<model_name>/')
@app.route('/<model_name>/<item_id>')
def rest_pages(model_name, item_id=None):
    if model_name in crud_url_models:
        model_class = crud_url_models[model_name]
        
        if item_id is None or session.query(exists().where(
                model_class.id == item_id)).scalar():
            return make_response(open(
                'summoner_stalker/templates/index.html').read())
    abort(404)

# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'img/favicon.ico')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
    
  
@socket.on('connect')
def on_connect():
    app.logger.info('Client connected!')
    get_current_ingame_status() # TODO: filter only send to newly connected client
    global thread
    # Start thread only if it hasn't been started yet
    if not thread.isAlive():
        app.logger.info('Starting Background Thread to poll api')
        thread = SummonerGamesThread()
        thread.start()

@app.route('/match/<summoner_id>')
def summoner_game(summoner_id):
    summoner = riotapi.get_summoner_by_id(summoner_id)
    match_list = summoner.match_list()
    match_ref = match_list[0] # last match
    match = match_ref.match(include_timeline=False) # timeline is a lot of data
    participants = match.participants
    participants_resp = []
    format_str = lambda s : str(s).split('.')[1].replace('_',' ')
    for participant in participants:
        participants_resp.append(dict(champion=participant.champion.name,
                                      side=format_str(participant.side),
                                      summoner=participant.summoner_name))
    response = dict(map_name=format_str(match.map),
                    duration=str(match.duration),
                    mode=format_str(match.mode),
                    type=format_str(match.type),
                    platform=format_str(match.platform),
                    queue=format_str(match.queue),
                    region=format_str(match.region),
                    season=format_str(match.season),
                    red_team_bans=len(match.red_team.bans),
                    blue_team_bans=len(match.blue_team.bans),
                    creation=match.creation,
                    participants=participants_resp)    
    return flask.jsonify(response)
