from datetime import datetime

from summoner_stalker.core import db
from summoner_stalker import app

class Summoner(db.Model):
    # id in this case is our internal one
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    # riot games api id
    api_id = db.Column(db.Integer, nullable=False)
    watched_date = db.Column(db.DateTime)

    def __init__(self, name, api_id, watched_date=None):
        # client will later get http 400 response if either below are None
        self.name = name
        self.api_id = api_id
        
        if watched_date is None:
            watched_date = datetime.utcnow()
        self.watched_date = watched_date

    def __repr__(self):
        return '<Summoner %r>' % self.name

# models for which we want to create API endpoints
app.config['API_MODELS'] = {'post': Summoner}

# models for which we want to create CRUD-style URL endpoints,
# and pass the routing onto our AngularJS application
app.config['CRUD_URL_MODELS'] = {'post': Summoner}
