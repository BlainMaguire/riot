
DEBUG = True # for demo purposes, should be false in production
SECRET_KEY = 'temporary_secret_key' # for flask, change to something a lot less guessable in production

RGAPI_REGION = 'EUW' # should be specific to where application is running
RGAPI_KEY = 'RGAPI-9af8c0d2-fcfe-4a93-b5cc-5d3b076ce6e7' # replace with working key

# for convenience for local machine on OSX/Linux, replace with production SQL Server URI
SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/summoner_stalker.db'
