# Summoner Stalker

Keep tabs on your favourite summoners and what they've been up to.

#### Features:

* uses web socket events to let the view know when summoner is now in game or not.
* uses web notifications so you can see this change without having to be on the page.
* when clicking into a summoner, you'll see their last match along with some stats (takes a while to pull the data)
* add / remove summoners

#### Technology used:

SQL Database: powered by SQLAlchemy for ORM, sqlitedb in /tmp is the default in config
Flask: Python web microframework
AngularJS: DOM Manipulation is a lot simpler when using something like this.
Socket.io: For websockets, and a decent fallback for browsers that don't have them
Twitter Bootstrap: To get something half decent looking out of the box.

#### Installation

You will need Python 2.7 installed.
[Pip](https://pip.pypa.io/en/stable/installing/) for installing required packages

[Virtualenv](https://virtualenv.pypa.io/en/stable/) for isolating your python environment

Python working with async web development. You'll need [libevent](http://libevent.org/) installed on your system. For MacOSX at minimum you'll need:

```sh
$ brew install libevent
```

On Ubuntu it's:

```sh
$ apt-get install libevent-dev
$ apt-get install python-all-dev
```

#### Running

* clone the repo
* create a virtual environment in the root, e.g. virtualenv env

In any terminals you plan to run scripts, do so from your new python environment:

```sh
$ source env/bin/activate
```

For the first time you activate your environment install the requirements with:

```sh
$ pip install -r requirements.txt
```

In the root of the project are some scripts for your convenience.

The API key in repo is not active (for obvious reasons). You'll need to set RGAPI_KEY in the config file. /summoner_stalker/settings.py

To create an empty database:

```sh
$ python manage_db.py create_db
```

To start the web server:
```sh
$ python runserver.py
```

While the web server is running, use the api to populate it with some data:
```sh
$ python manage_db.py seed_db --seedfile 'data/db_items.json'

```

Browse to http://localhost:5000 and you should see the landing page.

#### Decisions Made

I went for Python for the backend because the Cassiopeia seemed to be the best library out there for talking to Riot's API. I found it quite intuitive when I was testing it out.

I used Flask because it's pretty simple to just get something working quickly. I did try the restless extension for generating API crud endpoints as well.

Had I had more time, I would move out the background polling thread to emit events into a seperate process and let that emit events, backed by Redis.